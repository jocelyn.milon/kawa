package fr.afpa.kawa.ui.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import fr.afpa.kawa.R;
import fr.afpa.kawa.ui.home.HomeActivity;

public class MainActivity extends AppCompatActivity {


    // initialisation de variable
    private Timer myTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTimer = new Timer(); // instance de classe

        // déclaration d'un objet de type TimerTask
        TimerTask monTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("Main", "Lancer HomeActivity");

                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(myIntent);
                finish(); // ou android:noHistory="true" dans le Manifest
            }
        };

        myTimer.schedule(monTimerTask, 2000); // 2000 = durée en milliseconds = 2 secondes
    }
}
